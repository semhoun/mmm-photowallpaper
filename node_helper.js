"use strict";

const NodeHelper = require("node_helper");
const request = require("request");
const fs = require("fs");
const path = require("path");
const mime = require("mime-types");
const express = require("express");
const exif = require('jpeg-exif');
const sizeOf = require('image-size');
const local_images = "/modules/MMM-PhotoWallpaper/photos";

function shuffle(a, limit = -1) {
	var source = a.slice(0);
	var result = [];
	var i, j;
	var size = a.length;
	if (limit > 0 && limit < size) {
		size = limit;
	}

	for (i = a.length; i > 0; --i) {
		j = Math.floor(Math.random() * i);
		result.push(source[j]);
		source[j] = source[i];
	}

	return result.slice(0, limit - 1);
}

module.exports = NodeHelper.create({
	start: function() {
		console.log("Starting node helper for: " + this.name);
		this.cache = {};
		this.path_images = path.resolve(global.root_path + local_images);
		this.extraRoutes();
	},

	socketNotificationReceived: function(notification, payload) {
		if (notification === "FETCH_WALLPAPERS") {
			this.fetchWallpapers(payload);
		}
	},

	fetchWallpapers: function(config) {
		var cache_key = this.getCacheKey(config);
		var url;
		var method = "GET";
		var body = undefined;

		if (cache_key in this.cache &&
			config.maximumEntries <= this.cache[cache_key].images.length &&
			Date.now() < this.cache[cache_key].expires)
		{
			this.sendWallpaperUpdate(config);
			return;
		}

		var source = config.source.toLowerCase();
		if (source === "local") {
			this.fetchLocalData(config);
			return;
		} else if (source === "firetv") {
			this.fetchFireTV(config);
			return;
		} else if (source.startsWith("/r/")) {
			url = "https://www.reddit.com" + config.source + "/hot.json";
		} else if (source.startsWith("icloud:")) {
			method = "POST";
			url = "https://p04-sharedstreams.icloud.com/" + config.source.substring(7) + "/sharedstreams/webstream";
			body = '{"streamCtag":null}';
			this.iCloudState = "webstream";
		} else {
			url = "https://www.bing.com/HPImageArchive.aspx?format=js&idx=0&n=" + config.maximumEntries;
		}

		this.request(method, url, body, config);
	},

	request: function(method, url, body, config) {
		var self = this;

		request({
			url: url,
			method: method,
			headers: { "cache-control": "no-cache" },
			body: body,
		}, function(error, response, body) {
			if (error) {
				self.sendSocketNotification("FETCH_ERROR", { error: error });
				return console.error(" ERROR - MMM-Wallpaper: " + error);
			}
			if (response.statusCode < 400 && body.length > 0) {
				self.processResponse(response, JSON.parse(body), config);
			}
		});
	},

	// create routes for module manager.
	// recive request and send response
	extraRoutes: function() {
		this.expressApp.use("/MMM-PhotoWallpaper/photo", express.static(this.path_images));
	},

	sendWallpaperUpdate: function(config) {
		var cache_key = this.getCacheKey(config);

		this.sendSocketNotification("WALLPAPERS", {
			"source": config.source,
			"orientation": config.orientation,
			"images": this.cache[cache_key].images,
		});
	},

	processResponse: function(response, body, config) {
		var cache_key = this.getCacheKey(config);
		var images;

		var source = config.source.toLowerCase();
		if (source.startsWith("/r/")) {
			images = this.processRedditData(config, body);
		} else if (source.startsWith("icloud:")) {
			images = this.processiCloudData(response, body, config);
		} else {
			images = this.processBingData(config, body);
		}

		if (images.length === 0) {
			return;
		}

		this.cache[cache_key] = {
			"expires": Date.now() + config.updateInterval * 0.9,
			"images": images,
		};

		this.sendWallpaperUpdate(config);
	},

	processBingData: function(config, data) {
		var width = (config.orientation === "vertical") ? 1080 : 1920;
		var height = (config.orientation === "vertical") ? 1920 : 1080;
		var suffix = "_" + width + "x" + height + ".jpg";

		var images = [];
		for (var i in data.images) {
			var image = data.images[i];

			images.push({
				url: "https://www.bing.com" + image.urlbase + suffix,
				width: width,
				height: height,
				caption: image.copyright,
			});
		}

		return images;
	},

	processRedditData: function(config, data) {
		var images = [];
		for (var i in data.data.children) {
			var post = data.data.children[i];

			if (post.kind === "t3" && !post.data.pinned && !post.data.stickied) {
				var source = post.data.preview.images[0].source;

				images.push({
					url: source.url.replace("&amp;", "&"),
					width: source.width,
					height: source.height,
					caption: post.data.title,
				});

				if (images.length === config.maximumEntries) {
					break;
				}
			}
		}

		return images;
	},

	processiCloudData: function(response, body, config) {
		var album = config.source.substring(7);
		var images = [];

		if (this.iCloudState === "webstream") {
			if (response.statusCode === 330) {
				this.iCloudHost = body["X-Apple-MMe-Host"];
				this.request("POST", "https://" + this.iCloudHost + "/" + album + "/sharedstreams/webstream", '{"streamCtag":null}', config);
			} else if (response.statusCode === 200) {
				var photos = shuffle(body.photos).slice(0, config.maximumEntries);
				var photoGuids = photos.map((p) => { return p.photoGuid; });

				this.iCloudState = "webasseturls";
				this.iCloudMetadata = photos.reduce((o, p) => {
					if (p.derivatives.mediaAssetType === "video") {
						return o;
					}

					for (var d in p.derivatives) {
						var meta = p.derivatives[d];

						o[meta.checksum] = {
							width: meta.width,
							height: meta.height,
							caption: p.caption
						};
					}

					return o;
				}, {});

				this.request("POST", "https://" + this.iCloudHost + "/" + album + "/sharedstreams/webasseturls", JSON.stringify({"photoGuids": photoGuids}), config);
			}
		} else if (this.iCloudState === "webasseturls") {
			for (var guid in body.items) {
				var p = body.items[guid];
				var loc = body.locations[p.url_location];
				var host = loc.hosts[Math.floor(Math.random() * loc.hosts.length)];
				var meta = this.iCloudMetadata[guid];

				images.push({
					url: loc.scheme + "://" + host + p.url_path,
					width: meta.width,
					height: meta.height,
					caption: meta.caption,
				});
			}
		}

		return images;
	},

	fetchLocalData: function(config) {
		var files = [];
		var images = [];
		var enabledTypes = ["image/jpeg", "image/png", "image/gif"];

		var walk = function (dir, subdir) {
		var results = [];
		var list = fs.readdirSync(dir + '/' + subdir);
		list.forEach(function(file) {
			var path = subdir + file;
			var fpath = dir + '/' + path;
			var stat = fs.statSync(fpath);
			if (stat) {
				if (stat.isDirectory()) { 
					/* Recurse into a subdirectory */
					results = results.concat(walk(dir, path + '/'));
				}
				else { 
					/* Is a file */
					var type = mime.lookup(fpath);
					if (enabledTypes.indexOf(type) >= 0 && type !== false) {
						results.push(path);
					}
				}
			}
		});
		return results;
	}

		files = walk(this.path_images, '');

		files = shuffle(files, config.maximumEntries);
		for (var idx in files) {
			var file = files[idx];

			var info = exif.parseSync(this.path_images + '/' + file);
			var size = sizeOf(this.path_images + '/' + file);

			images.push({
				url: '/MMM-PhotoWallpaper/photo/' + file,
				width: size.width,
				height: size.height,
				caption: info.ImageDescription
			});
		}

		var cache_key = this.getCacheKey(config);
		this.cache[cache_key] = {
			"expires": Date.now() + config.updateInterval * 0.9,
			"images": images,
		};
		this.sendWallpaperUpdate(config);
	},

	fetchFireTv: function(config) {
		this.firetv = JSON.parse(fs.readFileSync(__dirname + "/firetv.json"));

		var cache_key = this.getCacheKey(config);
		this.cache[cache_key] = {
			"expires": Date.now() + config.updateInterval * 0.9,
			"images":  shuffle(self.firetv.images, config.maximumEntries)
		};
		this.sendWallpaperUpdate(config);
	},

	getCacheKey: function(config) {
		return config.source + "::" + config.orientation;
	},
});
