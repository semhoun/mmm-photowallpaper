# Module: MMM-PhotosWallpaper
The module allows you to add wallpapers from various online sources.  Useful for MagicMirror installations that aren't actually mirrors.

Based on https://github.com/kolbyjack/MMM-Wallpaper.git and https://github.com/roramirez/MMM-ImagesPhotos.git

## Installation

In your terminal, go to your MagicMirror's Module folder:
````
cd ~/MagicMirror/modules
````

Clone this repository:
````
git clone https://gitlab.com/semhoun/mmm-photowallpaper.git MMM-PhotoWallpaper
````

Configure the module in your `config.js` file.

**Note:** After starting the Mirror, it will take a few seconds before the wallpapers start to appear.

## Using the module

To use this module, add it to the modules array in the `config/config.js` file:
````javascript
modules: [
  {
    module: "MMM-PhotoWallpaper",
    position: "fullscreen_below",
    config: { // See "Configuration options" for more information.
      source: "bing",
      slideInterval: 60 * 1000 // Change slides every minute
    }
  }
]
````

## Configuration options

The following properties can be configured:

|Option|Default|Description|
|---|---|---|
|`source`|`"bing"`|The image source to use.  Supported sources:<br/>&nbsp;- "bing": cycles through the most recent daily wallpapers from Bing<br/>&nbsp;- "firetv": cycles through random selections of the FireTV wallpapers<br/>&nbsp;- "/r/&lt;subreddit&gt;": cycles through the most recent "hot" image posts from the subreddit<br/>&nbsp;- "icloud:&lt;album id&gt;": cycles through random selections of the specified album<br/>&nbsp;- "local": cycles through random selections of local images in photos directory, could be in subdirectories. photos/undefined is the default photo|
|`updateInterval`|`60 * 60 * 1000`|How often (in ms) to check the source for new wallpapers.|
|`slideInterval`|`5 * 60 * 1000`|How often (in ms) to change images.|
|`maximumEntries`|`10`|The maximum number of images to switch between from the source.|
|`filter`|`"grayscale(0.5) brightness(0.5)"`|The CSS filter to apply to the images, to improve readability of other modules' text|
|`orientation`|`"auto"`|The image orientation to retrieve.  Choices are "vertical", "horizontal", and "auto".|
|`imageFit`|`"cover"`|The image fit.  Choices are "cover" and "contain".|